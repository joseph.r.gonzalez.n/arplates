package com.example.arplates;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ScrollView;
import android.widget.TextView;

public class Parking extends Activity implements View.OnClickListener{

    int counter = 45;

    private float mx, my;
    private float curX, curY;


    Drawable colorOn ;

    Drawable colorOff ;

    TextView etCounter;

    Button park1,park2,park3,park4,park5,park6,park7,park8,park9,park10,park11,park12,park13,park14,park15;
    Button park16,park17,park18,park19,park20,park21,park22,park23,park24,park25,park26,park27,park28,park29,park30,park31;
    Button park32,park33,park34,park35,park36,park37,park38,park39,park40,park41,park42,park43,park44,park45;

    Boolean flag1 = true;

    private ScrollView vScroll;
    private HorizontalScrollView hScroll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.parking);

       colorOn  = ResourcesCompat.getDrawable(getResources(), R.drawable.park_on, null);

       colorOff  = ResourcesCompat.getDrawable(getResources(), R.drawable.park_off, null);

        etCounter = findViewById(R.id.tvSpots);
        etCounter.setText("Espacios disponibles: " + counter);

        park1 = findViewById(R.id.btn1);
        park2 =  findViewById(R.id.btn2);
        park3 =  findViewById(R.id.btn3 );
        park4 =  findViewById(R.id.btn4 );
        park5 =  findViewById(R.id.btn5 );
        park6 =  findViewById(R.id.btn6 );
        park7 =  findViewById(R.id.btn7 );
        park8 =  findViewById(R.id.btn8 );
        park9 =  findViewById(R.id.btn9 );
        park10 = findViewById(R.id.btn10);
        park11 = findViewById(R.id.btn11);
        park12 = findViewById(R.id.btn12);
        park13 = findViewById(R.id.btn13);
        park14 = findViewById(R.id.btn14);
        park15 = findViewById(R.id.btn15);
        park16 = findViewById(R.id.btn16);
        park17 = findViewById(R.id.btn17);
        park18 = findViewById(R.id.btn18);
        park19 = findViewById(R.id.btn19);
        park20 = findViewById(R.id.btn20);
        park21 = findViewById(R.id.btn21);
        park22 = findViewById(R.id.btn22);
        park23 = findViewById(R.id.btn23);
        park24 = findViewById(R.id.btn24);
        park25 = findViewById(R.id.btn25);
        park26 = findViewById(R.id.btn26);
        park27 = findViewById(R.id.btn27);
        park28 = findViewById(R.id.btn28);
        park29 = findViewById(R.id.btn29);
        park30 = findViewById(R.id.btn30);
        park31 = findViewById(R.id.btn31);
        park32 = findViewById(R.id.btn32);
        park33 = findViewById(R.id.btn33);
        park34 = findViewById(R.id.btn34);
        park35 = findViewById(R.id.btn35);
        park36 = findViewById(R.id.btn36);
        park37 = findViewById(R.id.btn37);
        park38 = findViewById(R.id.btn38);
        park39 = findViewById(R.id.btn39);
        park40 = findViewById(R.id.btn40);
        park41 = findViewById(R.id.btn41);
        park42 = findViewById(R.id.btn42);
        park43 = findViewById(R.id.btn43);
        park44 = findViewById(R.id.btn44);
        park45 = findViewById(R.id.btn45);



        park1.setOnClickListener(this);
        park2.setOnClickListener(this);
        park3.setOnClickListener(this);
        park4.setOnClickListener(this);
        park5.setOnClickListener(this);
        park6.setOnClickListener(this);
        park7.setOnClickListener(this);
        park8.setOnClickListener(this);
        park9.setOnClickListener(this);
        park10.setOnClickListener(this);
        park11.setOnClickListener(this);
        park12.setOnClickListener(this);
        park13.setOnClickListener(this);
        park14.setOnClickListener(this);
        park15.setOnClickListener(this);
        park16.setOnClickListener(this);
        park17.setOnClickListener(this);
        park18.setOnClickListener(this);
        park19.setOnClickListener(this);
        park20.setOnClickListener(this);
        park21.setOnClickListener(this);
        park22.setOnClickListener(this);
        park23.setOnClickListener(this);
        park24.setOnClickListener(this);
        park25.setOnClickListener(this);
        park26.setOnClickListener(this);
        park27.setOnClickListener(this);
        park28.setOnClickListener(this);
        park29.setOnClickListener(this);
        park30.setOnClickListener(this);
        park31.setOnClickListener(this);
        park32.setOnClickListener(this);
        park33.setOnClickListener(this);
        park34.setOnClickListener(this);
        park35.setOnClickListener(this);
        park36.setOnClickListener(this);
        park37.setOnClickListener(this);
        park38.setOnClickListener(this);
        park39.setOnClickListener(this);
        park40.setOnClickListener(this);
        park41.setOnClickListener(this);
        park42.setOnClickListener(this);
        park43.setOnClickListener(this);
        park44.setOnClickListener(this);
        park45.setOnClickListener(this);



        park1.setBackground(colorOn);
        park2.setBackground(colorOn);
        park3.setBackground(colorOn);
        park4.setBackground(colorOn);
        park5.setBackground(colorOn);
        park6.setBackground(colorOn);
        park7.setBackground(colorOn);
        park8.setBackground(colorOn);
        park9.setBackground(colorOn);
        park10.setBackground(colorOn);
        park11.setBackground(colorOn);
        park12.setBackground(colorOn);
        park13.setBackground(colorOn);
        park14.setBackground(colorOn);
        park15.setBackground(colorOn);
        park16.setBackground(colorOn);
        park17.setBackground(colorOn);
        park18.setBackground(colorOn);
        park19.setBackground(colorOn);
        park20.setBackground(colorOn);
        park21.setBackground(colorOn);
        park22.setBackground(colorOn);
        park23.setBackground(colorOn);
        park24.setBackground(colorOn);
        park25.setBackground(colorOn);
        park26.setBackground(colorOn);
        park27.setBackground(colorOn);
        park28.setBackground(colorOn);
        park29.setBackground(colorOn);
        park30.setBackground(colorOn);
        park31.setBackground(colorOn);
        park32.setBackground(colorOn);
        park33.setBackground(colorOn);
        park34.setBackground(colorOn);
        park35.setBackground(colorOn);
        park36.setBackground(colorOn);
        park37.setBackground(colorOn);
        park38.setBackground(colorOn);
        park39.setBackground(colorOn);
        park40.setBackground(colorOn);
        park41.setBackground(colorOn);
        park42.setBackground(colorOn);
        park43.setBackground(colorOn);
        park44.setBackground(colorOn);
        park45.setBackground(colorOn);


        vScroll = (ScrollView) findViewById(R.id.vScroll);
        hScroll = (HorizontalScrollView) findViewById(R.id.hScroll);

    }




    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //float curX, curY;

        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:
                mx = event.getX();
                my = event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                curX = event.getX();
                curY = event.getY();
                vScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                hScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                mx = curX;
                my = curY;
                break;
            case MotionEvent.ACTION_UP:
                curX = event.getX();
                curY = event.getY();
                vScroll.scrollBy((int) (mx - curX), (int) (my - curY));
                hScroll.scrollBy((int) (mx - curX), (int) (my - curY));

                break;
        }

        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn1 :
                if(park1.getBackground().equals(colorOn)){
                    park1.setBackground(colorOff);
                    counter--;
                }else{
                    park1.setBackground(colorOn);
                    counter++;
                }
                flag1 = !flag1;
                break;
            case R.id.btn2 :
                if(park2.getBackground().equals(colorOn)){
                    park2.setBackground(colorOff);
                    counter--;
                }else{
                    park2.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn3 :
                if(park3.getBackground().equals(colorOn)){
                    park3.setBackground(colorOff);
                    counter--;
                }else{
                    park3.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn4 :
                if(park4.getBackground().equals(colorOn)){
                    park4.setBackground(colorOff);
                    counter--;
                }else{
                    park4.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn5 :
                if(park5.getBackground().equals(colorOn)){
                    park5.setBackground(colorOff);
                    counter--;
                }else{
                    park5.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn6 :
                if(park6.getBackground().equals(colorOn)){
                    park6.setBackground(colorOff);
                    counter--;
                }else{
                    park6.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn7 :
                if(park7.getBackground().equals(colorOn)){
                    park7.setBackground(colorOff);
                    counter--;
                }else{
                    park7.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn8 :
                if(park8.getBackground().equals(colorOn)){
                    park8.setBackground(colorOff);
                    counter--;
                }else{
                    park8.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn9 :
                if(park9.getBackground().equals(colorOn)){
                    park9.setBackground(colorOff);
                    counter--;
                }else{
                    park9.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn10:
                if(park10.getBackground().equals(colorOn)){
                    park10.setBackground(colorOff);
                    counter--;
                }else{
                    park10.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn11:
                if(park11.getBackground().equals(colorOn)){
                    park11.setBackground(colorOff);
                    counter--;
                }else{
                    park11.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn12:
                if(park12.getBackground().equals(colorOn)){
                    park12.setBackground(colorOff);
                    counter--;
                }else{
                    park12.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn13:
                if(park13.getBackground().equals(colorOn)){
                    park13.setBackground(colorOff);
                    counter--;
                }else{
                    park13.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn14:
                if(park14.getBackground().equals(colorOn)){
                    park14.setBackground(colorOff);
                    counter--;
                }else{
                    park14.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn15:
                if(park15.getBackground().equals(colorOn)){
                    park15.setBackground(colorOff);
                    counter--;
                }else{
                    park15.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn16:
                if(park16.getBackground().equals(colorOn)){
                    park16.setBackground(colorOff);
                    counter--;
                }else{
                    park16.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn17:
                if(park17.getBackground().equals(colorOn)){
                    park17.setBackground(colorOff);
                    counter--;
                }else{
                    park17.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn18:
                if(park18.getBackground().equals(colorOn)){
                    park18.setBackground(colorOff);
                    counter--;
                }else{
                    park18.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn19:
                if(park19.getBackground().equals(colorOn)){
                    park19.setBackground(colorOff);
                    counter--;
                }else{
                    park19.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn20:
                if(park20.getBackground().equals(colorOn)){
                    park20.setBackground(colorOff);
                    counter--;
                }else{
                    park20.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn21:
                if(park21.getBackground().equals(colorOn)){
                    park21.setBackground(colorOff);
                    counter--;
                }else{
                    park21.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn22:
                if(park22.getBackground().equals(colorOn)){
                    park22.setBackground(colorOff);
                    counter--;
                }else{
                    park22.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn23:
                if(park23.getBackground().equals(colorOn)){
                    park23.setBackground(colorOff);
                    counter--;
                }else{
                    park23.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn24:
                if(park24.getBackground().equals(colorOn)){
                    park24.setBackground(colorOff);
                    counter--;
                }else{
                    park24.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn25:
                if(park25.getBackground().equals(colorOn)){
                    park25.setBackground(colorOff);
                    counter--;
                }else{
                    park25.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn26:
                if(park26.getBackground().equals(colorOn)){
                    park26.setBackground(colorOff);
                    counter--;
                }else{
                    park26.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn27:
                if(park27.getBackground().equals(colorOn)){
                    park27.setBackground(colorOff);
                    counter--;
                }else{
                    park27.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn28:
                if(park28.getBackground().equals(colorOn)){
                    park28.setBackground(colorOff);
                    counter--;
                }else{
                    park28.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn29:
                if(park29.getBackground().equals(colorOn)){
                    park29.setBackground(colorOff);
                    counter--;
                }else{
                    park29.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn30:
                if(park30.getBackground().equals(colorOn)){
                    park30.setBackground(colorOff);
                    counter--;
                }else{
                    park30.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn31:
                if(park31.getBackground().equals(colorOn)){
                    park31.setBackground(colorOff);
                    counter--;
                }else{
                    park31.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn32:
                if(park32.getBackground().equals(colorOn)){
                    park32.setBackground(colorOff);
                    counter--;
                }else{
                    park32.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn33:
                if(park33.getBackground().equals(colorOn)){
                    park33.setBackground(colorOff);
                    counter--;
                }else{
                    park33.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn34:
                if(park34.getBackground().equals(colorOn)){
                    park34.setBackground(colorOff);
                    counter--;
                }else{
                    park34.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn35:
                if(park35.getBackground().equals(colorOn)){
                    park35.setBackground(colorOff);
                    counter--;
                }else{
                    park35.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn36:
                if(park36.getBackground().equals(colorOn)){
                    park36.setBackground(colorOff);
                    counter--;
                }else{
                    park36.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn37:
                if(park37.getBackground().equals(colorOn)){
                    park37.setBackground(colorOff);
                    counter--;
                }else{
                    park37.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn38:
                if(park38.getBackground().equals(colorOn)){
                    park38.setBackground(colorOff);
                    counter--;
                }else{
                    park38.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn39:
                if(park39.getBackground().equals(colorOn)){
                    park39.setBackground(colorOff);
                    counter--;
                }else{
                    park39.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn40:
                if(park40.getBackground().equals(colorOn)){
                    park40.setBackground(colorOff);
                    counter--;
                }else{
                    park40.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn41:
                if(park41.getBackground().equals(colorOn)){
                    park41.setBackground(colorOff);
                    counter--;
                }else{
                    park41.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn42:
                if(park42.getBackground().equals(colorOn)){
                    park42.setBackground(colorOff);
                    counter--;
                }else{
                    park42.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn43:
                if(park43.getBackground().equals(colorOn)){
                    park43.setBackground(colorOff);
                    counter--;
                }else{
                    park43.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn44:
                if(park44.getBackground().equals(colorOn)){
                    park44.setBackground(colorOff);
                    counter--;
                }else{
                    park44.setBackground(colorOn);
                    counter++;
                }
                break;
            case R.id.btn45:
                if(park45.getBackground().equals(colorOn)){
                    park45.setBackground(colorOff);
                    counter--;
                }else{
                    park45.setBackground(colorOn);
                    counter++;
                }
                break;



        }

        etCounter.setText("Espacios disponibles: " + counter);

    }


}