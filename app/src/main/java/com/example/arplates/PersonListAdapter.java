package com.example.arplates;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.lucasurbas.listitemview.ListItemView;

import java.util.ArrayList;

public class PersonListAdapter extends ArrayAdapter<Person> {

    private static final String TAG = "PersonListAdapter";

    private Context mContext;
    int mResource;

    public PersonListAdapter(Context context, int resource, ArrayList<Person> objects) {
        super(context, resource,objects);
        mContext = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        //Get person information
        String name = getItem(position).getName();
        String plate = getItem(position).getPlate();
        String time = getItem(position).getTime();

        //Create the person object with the information
        Person person = new Person(name, plate, time);

        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        convertView = layoutInflater.inflate(mResource, parent, false);

        ListItemView listItemView = (ListItemView) convertView.findViewById(R.id.list_item_view);

        listItemView.setTitle(name + " "+ plate);
        listItemView.setSubtitle(time);

        return convertView;
    }
}
