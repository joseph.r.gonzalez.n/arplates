package com.example.arplates;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.example.arplates.Books.app.Books.Books;

public class Menu extends AppCompatActivity implements View.OnClickListener{

    ImageButton camera, stats, parking, historial, money;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);


        camera = findViewById(R.id.ibCamera);
        stats = findViewById(R.id.ibStats);
        parking = findViewById(R.id.ibParking);


        historial = findViewById(R.id.ibHistorial);
        money = findViewById(R.id.ibMoney);

        camera.setOnClickListener(this);
        stats.setOnClickListener(this);
        parking.setOnClickListener(this);

        historial.setOnClickListener(this);
        money.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibCamera:
                startActivity(new Intent(Menu.this, Books.class));
                break;

            case R.id.ibStats:
                startActivity(new Intent(Menu.this, Stats.class));
                break;

            case R.id.ibParking:
                startActivity(new Intent(Menu.this, Parking.class));
                break;

            case R.id.ibHistorial:
                startActivity(new Intent(Menu.this, Historial.class));
                break;

            case R.id.ibMoney:
                startActivity(new Intent(Menu.this, Money.class));
                break;

        }

    }
}
