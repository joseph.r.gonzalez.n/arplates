package com.example.arplates;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;

import androidx.annotation.Nullable;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;

import java.util.ArrayList;

public class Money extends AppCompatActivity {

    private static String TAG = "MoneyActivity";

    private float[] yData = {65.0f, 35.0f, 450.0f, 150.0f, 5000.0f, 2000.0f };
    private String[] xData = {"Actual", "Restante"};

    protected Typeface tfRegular;
    protected Typeface tfLight;



    PieChart ch_day, ch_month, ch_year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_money);

        Log.d(TAG, "onCreate: starting the code char");


        tfRegular = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");
        tfLight = Typeface.createFromAsset(getAssets(), "OpenSans-Light.ttf");

        ch_day  = (PieChart) findViewById(R.id.ch_day);
        ch_month = (PieChart) findViewById(R.id.ch_month);
        ch_year = (PieChart) findViewById(R.id.ch_year);

        configureChar(ch_day,"Día:\n 26/05/2019 \n Objetivo: 100$", 0, 1);
        configureChar(ch_month,"Mes:\n Septiembre \n Objetivo: 600$", 2, 3);
        configureChar(ch_year,"Año:\n 2019 \n Objetivo: 7000$", 4, 5);



    }

    private void configureChar(PieChart chart, String midle_message, int i, int j) {

        chart.setUsePercentValues(false);
        chart.getDescription().setEnabled(false);
        chart.setExtraOffsets(5, 10, 5, 5);

        chart.setDragDecelerationFrictionCoef(0.95f);

        chart.setCenterTextTypeface(tfLight);
        chart.setCenterTextSize(10);
        chart.setCenterText(generateCenterSpannableText(midle_message));

        chart.setDrawHoleEnabled(true);
        chart.setHoleColor(Color.WHITE);

        chart.setTransparentCircleColor(Color.WHITE);
        chart.setTransparentCircleAlpha(110);

        chart.setHoleRadius(58f);
        chart.setTransparentCircleRadius(61f);

        chart.setDrawCenterText(true);

        chart.setRotationAngle(0);
        chart.setRotationEnabled(true);
        chart.setHighlightPerTapEnabled(true);


        chart.animateY(1400, Easing.EaseInOutQuad);


        // entry label styling
        chart.setEntryLabelColor(Color.BLACK);
        chart.setEntryLabelTypeface(tfRegular);
        chart.setEntryLabelTextSize(12f);

        chart.getLegend().setEnabled(false);

        setData(i, j, chart);
    }

    private SpannableString generateCenterSpannableText(String text) {

        SpannableString s = new SpannableString(text);
        s.setSpan(new RelativeSizeSpan(1.7f), 0, 6, 0);
        s.setSpan(new StyleSpan(Typeface.ITALIC), 6, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), 6, s.length(), 0);
        return s;
    }

    private void setData(int i, int j, PieChart chart) {
        ArrayList<PieEntry> entries = new ArrayList<>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        entries.add(new PieEntry(yData[i], xData[0], getResources().getDrawable(R.drawable.star)));
        entries.add(new PieEntry(yData[j], xData[1], getResources().getDrawable(R.drawable.star)));
//        entries.add(new PieEntry(32, xData[2], getResources().getDrawable(R.drawable.star)));


        PieDataSet dataSet = new PieDataSet(entries, "Items");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<>();



        if(i == 0){
            for ( int k = 0; k < 2; k++){
                colors.add(Colors.PRIMARY_COLORS[k]);
            }
        }else if(i == 2){
            for ( int k = 2; k < 4; k++){
                colors.add(Colors.PRIMARY_COLORS[k]);
            }
        }else{
            for ( int k = 4; k < 6; k++){
                colors.add(Colors.PRIMARY_COLORS[k]);
            }
        }



        /*for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);
*/
        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter(chart));
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);
        data.setValueTypeface(tfLight);
        chart.setData(data);

        // undo all highlights
        chart.highlightValues(null);

        chart.invalidate();
    }
}
