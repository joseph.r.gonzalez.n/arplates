package com.example.arplates;

public class Person {
    private String name;
    private String plate;
    private String time;

    public Person(String name, String plate, String time){
        this.name = name;
        this.plate = plate;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
