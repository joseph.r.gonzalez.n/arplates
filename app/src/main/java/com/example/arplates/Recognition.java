package com.example.arplates;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Recognition extends AppCompatActivity implements CvCameraViewListener2 {

    //Tag para el debug de la aplicacion y esta actividad
    private static final String TAG = "OCR Recognition: ";

    //Instancia para almacenar la data entrenada de Tesseract
    private static Recognition instance;


    private CameraBridgeViewBase mOpenCvCameraView;

    //Instancia de la clase TessOCR para reconocer los caracteres usando Tesseract
    private TessOCR mTessOCR;



    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    public Recognition() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");

        // Pedir permisos para utilizar la camara
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    1);

            finish();
        }

        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar

        setContentView(R.layout.recognition);

        //Store the trained data in the device
        instance = this;
        copyTestDataForTextRecognition();

        //Init the TessOCR with the data train and the language
        String data_path = Recognition.instance.getTessDataParentDirectory();
        String language = "eng";
        mTessOCR = new TessOCR(data_path, language);


        mOpenCvCameraView = findViewById(R.id.tutorial1_activity_java_surface_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    private Mat output_img = null;
    private Mat gray = null;
    private Mat crop_img = null;
    private Mat bilateral = null;
    private Mat overlay = null;
    private Mat result = null;
    private Mat edged = null;
    private List<MatOfPoint> contours = null;
    private Mat hierarchy = null;
    private boolean detected = false;

    public void onCameraViewStarted(int width, int height) {
    }

    public void onCameraViewStopped() {
        if (gray != null){
            gray.release();
            output_img.release();
            bilateral.release();
            crop_img.release();
            overlay.release();
            edged.release();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {

        //Dimension of the big image
        double height = inputFrame.rgba().size().height;
        double width = inputFrame.rgba().size().width;

        //Dimension of crop
        int h_crop = 300;
        int w_crop = 500;
        int y = (int) (height/2) - (int)(h_crop/2);
        int x = (int) (width/2) - (int) (w_crop/2);

        //Copy of the entry frame
        output_img = inputFrame.rgba().clone();
        result = inputFrame.rgba();

        //Cropped Image
        Rect rectCrop = new Rect(x,y,w_crop,h_crop);
        crop_img = new Mat(output_img.clone(), rectCrop);

        //Draw a rectangle to see the focus of plate
        Imgproc.rectangle(crop_img,rectCrop,new Scalar(0, 0, 0),3);
        Imgproc.rectangle(result,rectCrop,new Scalar(0, 0, 0),3);


        //Reduce Opacity
//        double alpha = 0.5;
//        overlay = inputFrame.rgba().clone();
//        Rect rectOverlay = new Rect(0,0,(int) width,(int) height);
//        Imgproc.rectangle(overlay,rectOverlay,new Scalar(255, 0, 0), -1);
//        Core.addWeighted(overlay,alpha,inputFrame.rgba(),1-alpha,0,result);


        //Gray color to cropped image
        gray = new Mat(w_crop,h_crop, CvType.CV_8UC2);
        Imgproc.cvtColor(crop_img,gray,Imgproc.COLOR_RGB2GRAY);

        //Bilateral Filter
        bilateral = new Mat(w_crop,h_crop, CvType.CV_8UC2);
        Imgproc.bilateralFilter(gray,bilateral,1,50,120);

        //Canny Edge Detection
        edged = new Mat(w_crop,h_crop, CvType.CV_8UC2);
        Imgproc.Canny(bilateral, edged,200,220);

        //Find Contours
        contours = new ArrayList<>();
        hierarchy = new Mat(w_crop,h_crop, CvType.CV_8UC2);
        Imgproc.findContours(edged.clone(),contours,hierarchy,Imgproc.RETR_EXTERNAL,Imgproc.CHAIN_APPROX_SIMPLE);


        //Ordena los contornos de mayor a menor
        contours.sort(new Comparator<MatOfPoint>() {
            @Override
            public int compare(MatOfPoint mp1, MatOfPoint mp2) {
                if(Imgproc.contourArea(mp1) == Imgproc.contourArea(mp2)){
                    return 0;
                }
                return Imgproc.contourArea(mp2) > Imgproc.contourArea(mp1) ? 1 : -1;
            }
        });


        //Iterar por todo los contornos encontrados y tomar el mayor que se aproxime
        MatOfPoint2f screenCnt = new MatOfPoint2f();
        for (MatOfPoint cont: contours ) {
            MatOfPoint2f cont_new = new MatOfPoint2f(cont.toArray());

            if(Imgproc.contourArea(cont) > 5000){
                double arc_len = Imgproc.arcLength(cont_new,true);

                MatOfPoint2f approx = new MatOfPoint2f();
                Imgproc.approxPolyDP(cont_new,approx,0.012  *arc_len,true);

                if(approx.toList().size() == 4){
                    screenCnt = approx;
                    break;
                }
            }

        }

        //Si no se encuentra el contorno envio un mensaje
        if(screenCnt.empty()){
            detected = false;
            Log.e("Detection: ","No contour detected");
        }else{
            detected = true;
        }

        //Si detecto el contorno
        if(detected){

            //Cambio de tipo de dato del contorno
            MatOfPoint approxf1 = new MatOfPoint();
            screenCnt.convertTo(approxf1, CvType.CV_32S);
            List<MatOfPoint> contourTemp = new ArrayList<>();
            contourTemp.add(approxf1);

            //Dibujo el contorno en la pantalla
            Imgproc.drawContours(crop_img,contourTemp,-1, new Scalar( 0, 255, 0), 2);

            //Copio la imagen cortada a la original para mostrar la imagen completa
            crop_img.copyTo(result.submat(rectCrop));

                Rect rect_contour = Imgproc.boundingRect(screenCnt);

            int value_erase_ecuador = 56;
            int temp_value = rect_contour.y + value_erase_ecuador;
            if(temp_value < h_crop && rect_contour.height - value_erase_ecuador > 0) {
                rect_contour.y = temp_value;
                rect_contour.height = rect_contour.height - value_erase_ecuador;
                Mat only_letter_img = new Mat(gray, rect_contour);

                Imgproc.rectangle(crop_img,rect_contour,new Scalar(0, 0, 255),3);

                //Convierto la imagen cropped a un bitmap para usar tesseract y detectar los caracteres
                Bitmap bmp_result = null;
                try {
                    bmp_result = Bitmap.createBitmap(only_letter_img.cols(), only_letter_img.rows(), Bitmap.Config.ARGB_8888);
                    Utils.matToBitmap(only_letter_img, bmp_result);
                    String resultado_placa = mTessOCR.getOCRResult(bmp_result);
                    if (resultado_placa.length() == 7 || resultado_placa.length() == 8 || resultado_placa.length() == 9) {
                        Log.d(TAG, "PLACA DETECTADA: " + resultado_placa);
                        Imgproc.putText(result,resultado_placa,new Point(0,100),Imgproc.FONT_HERSHEY_PLAIN,5,new Scalar(0,0,255),5);
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
            }

            //Copio la imagen cortada a la original para mostrar la imagen completa
            crop_img.copyTo(result.submat(rectCrop));
        }

        return result;

    }

/*
    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {

        Imgproc.cvtColor(inputFrame.rgba(),gray,Imgproc.COLOR_RGB2GRAY);

        //Bilateral Filter
        Imgproc.bilateralFilter(gray,bilateral,1,50,120);

        //Canny Edge Detection
        Imgproc.Canny(bilateral, bilateral,200,220);

        //Morphology Change
        Imgproc.morphologyEx(bilateral,bilateral,Imgproc.MORPH_CLOSE,kernel);

        //Find Contours
        Imgproc.findContours(bilateral,contours,hierarchy,Imgproc.RETR_EXTERNAL,Imgproc.CHAIN_APPROX_SIMPLE);

        //Variable de resultado
        result = inputFrame.rgba();

        //
        for (MatOfPoint cont: contours ) {
            cont_new = new MatOfPoint2f(cont.toArray());
            if(Imgproc.contourArea(cont) > 2500){

                double arc_len = Imgproc.arcLength(cont_new,true);

                approx = new MatOfPoint2f();
                Imgproc.approxPolyDP(cont_new,approx,0.1*arc_len,true);

                Log.d("LEN APROX: ",String.valueOf(approx.toList()));

                if(approx.toList().size() == 4){
                    IS_FOUND = true;
                    h = Calib3d.findHomography(approx,corners_points);
                    Imgproc.warpPerspective(inputFrame.rgba(),out,h,new Size((int)( _width + _margin * 2 ),(int)( _height + _margin * 2 )));

                    approxf1 = new MatOfPoint();


                    approx.convertTo(approxf1, CvType.CV_32S);
                    contourTemp = new ArrayList<>();
                    contourTemp.add(approxf1);

                    Log.d("DETECT: ", "detecto");

                    Imgproc.drawContours(result,contourTemp,-1, new Scalar( 255, 0, 0), 2);
                }

            }
        }

        return result;
    }
*/


    private void copyTestDataForTextRecognition(){
        Runnable run = new Runnable() {
            @Override
            public void run() {
                AssetManager assetManager = Recognition.instance.getAssets();
                OutputStream out = null;
                try {
                    InputStream in = assetManager.open("eng.traineddata");
                    String tesspath = instance.tessDataPath();
                    File tessFolder = new File(tesspath);
                    if(!tessFolder.exists()){
                        tessFolder.mkdir();
                    }
                    String tessData = tesspath+"/eng.traineddata";
                    File tessFile = new File(tessData);
                    if(!tessFile.exists()){
                        out = new FileOutputStream(tessData);
                        byte[] buffer = new byte[1024];
                        int read = in.read(buffer);
                        while (read != 1){
                            out.write(buffer,0,read);
                            read = in.read(buffer);
                        }
                        Log.d(TAG,"Tess File Created");

                    }else{
                        Log.d(TAG,"Tess File Exist");
                    }


                } catch (IOException e) {
                    Log.e(TAG, "Couldn't copy with the following error:" + e.toString());
                } finally {
                    try {
                        if(out!=null){
                            out.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        new Thread(run).start();
    }

    private String tessDataPath() {
        return Recognition.instance.getExternalFilesDir(null)+"/tessdata/";
    }

    private String getTessDataParentDirectory(){
        return Recognition.instance.getExternalFilesDir(null).getAbsolutePath();
    }
}
