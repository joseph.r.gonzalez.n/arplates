/*===============================================================================
Copyright (c) 2016,2018 PTC Inc. All Rights Reserved.

Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.
===============================================================================*/

package com.example.arplates.Books.app.Books;

import android.graphics.Bitmap;


// A support class encapsulating the info for one book
public class Book
{
    private String plate;
    private String name;
    private String ratingAvg;
    private String targetId;
    private Bitmap thumb;
    private String telephone_number;
    
    
    public Book()
    {
        
    }
    
    
    public String getPlate()
    {
        return plate;
    }
    
    
    public void setPlate(String plate)
    {
        this.plate = plate;
    }
    
    
    public String getName()
    {
        return name;
    }
    
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    
    public String getRatingAvg()
    {
        return ratingAvg;
    }
    
    
    public void setRatingAvg(String ratingAvg)
    {
        this.ratingAvg = ratingAvg;
    }   
    
    public String getTargetId()
    {
        return targetId;
    }
    
    
    public void setTargetId(String targetId)
    {
        this.targetId = targetId;
    }
    
    
    public Bitmap getThumb()
    {
        return thumb;
    }
    
    
    public void setThumb(Bitmap thumb)
    {
        this.thumb = thumb;
    }
    
    
    public String getTelephoneNumber()
    {
        return telephone_number;
    }
    
    
    public void setTelephoneNumber(String telephone_number)
    {
        this.telephone_number = telephone_number;
    }
    
    
    public void recycle()
    {
        // Cleans the Thumb bitmap variable
        thumb.recycle();
        thumb = null;
    }
}
