package com.example.arplates;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.support.v7.widget.Toolbar;

import com.lucasurbas.listitemview.ListItemView;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;

public class Historial extends AppCompatActivity {

    private static final String TAG = "Historial";
    private PersonListAdapter adapter;
    private MaterialSearchView searchView;
    private ArrayList<Person> people;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial);

        Log.d(TAG,"onCreate: Started");




        //Configurar barra de busqueda
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Historial");
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));

        //Create person object

        Person laura = new Person("Laura Pazmiño", "(CTXI-784)","08h00-14h00");
        Person hector = new Person("Hector Garcia", "(XYDI-644)","08h00-17h00" );
        Person diego = new Person("Diego Cuenca", "(PXYA-218)","08h00-14h00");
        Person walter = new Person("Walter Blanco", "(TXY3-754)", "08h00-14h00");

        people = new ArrayList<Person>();
        people.add(laura);
        people.add(hector);
        people.add(diego);
        people.add(walter);

        listView = (ListView) findViewById(R.id.lvHistorial);
        adapter = new PersonListAdapter(this, R.layout.list_item_layout, people);
        listView.setAdapter(adapter);

        searchView =  (MaterialSearchView) findViewById(R.id.search_view);

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {
                listView = (ListView) findViewById(R.id.lvHistorial);
                adapter = new PersonListAdapter(Historial.this, R.layout.list_item_layout, people);
                listView.setAdapter(adapter);
            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if(newText != null && !newText.isEmpty()){
                    ArrayList<Person> lstFound = new ArrayList<Person>();
                    for (Person item:people){

                        String name = item.getName();
                        if(name.contains(newText)){
                            lstFound.add(item);
                        }

                    }

                    adapter = new PersonListAdapter(Historial.this, R.layout.list_item_layout, lstFound);
                    listView.setAdapter(adapter);
                }else{
                    adapter = new PersonListAdapter(Historial.this, R.layout.list_item_layout, people);
                    listView.setAdapter(adapter);
                }
                return true;
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_item,menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);

        return true;
    }


}
